const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const url = require('url');
const path = require('path')

// En produccion no quiero el electron-reload. 



  //  Tuve que comentar el electron-reload y sacarlo del package.json, porque me daba problemas en la distribucion final.

if(!app.isPackaged){
    require('electron-reload')(__dirname, {
        electron: path.join(__dirname, '../node_modules', '.bin', 'electron')
    })
}


// app es la aplicacion en si, BrowserWindow es la manera en la que vamos a poder crear ventanas.

let mainWindow; // Esta ventana deberia tener una ventana global, para que al cerrarse libere los recursos de la pc bien.
let newProductWindow;

// El evento on Ready es cuando la aplicacion este lista.
app.on('ready', () => {

    mainWindow = new BrowserWindow({
        // En el video deja el BrowserWindor vacio, pero si no pongo este objeto no funciona la
        //  integracion con NodeJs en la consola. (https://stackoverflow.com/questions/44391448/electron-require-is-not-defined)
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        }
    })

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/index.html'),
        protocol: 'file',
        slashes: true
    }));


    const mainMenu = Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(mainMenu);

    mainWindow.on('closed', () => {
        app.quit();
    })
});

const createNewProductWindow = () => {

    newProductWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        },
        width: 400,
        height: 330,
        title: 'Add a new product',
    })

    //newProductWindow.setMenu(null); -> Con esta linea elimino el menu de arriba de la ventana extra.
    newProductWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/new-product.html'),
        protocol: 'file',
        slashes: true
    }));

}

ipcMain.on('product:new', (e, newProduct) => {
    
    mainWindow.webContents.send('product:new', newProduct);
    newProductWindow.close();

})

const templateMenu = [
    {
        label: 'File',
        submenu: [
            {
                label: 'New product',
                accelerator: 'Ctrl+N',
                click(){
                    createNewProductWindow();
                }
            },
            {
                label: 'Remove All Products',
                click() {
                    mainWindow.webContents.send('products:removeAll')
                }
            },
            {
                label: 'Exit',
                accelerator: process.platform == 'darwin' ? 'command+Q' : 'Ctrl+Q', // Darwin significa Mac, platform devuelve el SO donde se ejecuta la app.
                click() {
                    app.quit();
                }
            }
        ]
    }
]

// Si es MAC, agrega el nombre de la app al principio del menu.
if(process.platform === 'darwin'){
    templateMenu.unshift({
        label: app.getName()
    })
}

// DEVTOOLS

//if(process.env.NODE_ENV !== 'production'){ -> En el tutorial usa este if, pero asi no funciona cuando hago el packaging.
if(!app.isPackaged){
    templateMenu.push({
        label: 'DevTools',
        submenu: [
            {
                label: 'Show/Hide DevTools',
                accelerator: 'Ctrl+D',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
            }
        ]
    })
}